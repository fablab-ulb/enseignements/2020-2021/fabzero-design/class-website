## Comment partir d'un objet pour en créer un autre ? ##


C'était le thème du cours de Design de la faculté d'architecture de l'ULB cette année. Le **Design Museum Brussels** a servi de socle pour cette première approche du design d'objets pour de futurs architectes.

Le premier jour, les étudiants ont visité le musée, sans guide.
Nous leur avons demandé de choisir un objet parmi les 500 objets exposés, sur un coup de cœur.
C'est à leur intuition que nous avons parlé. Rien du déroulement du cours ne leur avait été dévoilé avant cette visite. Ils se sont présentés aux autres étudiants du cours et ont décrit leur objet.

La première partie de l'apprentissage, appelée **FabZero**, a commencé  avec une série de cours menés par des "mentors", chacun choisi pour ses connaissances approfondies des sujets enseignés dans des disciplines différentes.
Ils sont restés disponibles pour aider les étudiants dans leurs domaines respectifs. Cette partie du travail est en quelque sorte une boîte à outils qui donne accès au Fablab et initie à sa philosophie.

Il s’agit tout d'abord d’apprendre à documenter son travail et à le partager avec les autres, cette étape est essentielle pour la formation d'un groupe d'étudiants qui apprennent les uns des autres.
Un logiciel de dessin est l'outil de conception et de visualisation commun à tous pour que l'outil donne à chacun les mêmes chances de pouvoir concevoir et produire ce qu'il imagine.

Et puis il y a les machines: imprimantes 3d, laser, défonceuse numérique, elles sont toutes apprises dans leur fonctionnement et limites pour que les étudiants sachent ce qu'ils peuvent réaliser et pour que les projets puissent également être partagés et soient reproduisibles.

![](./img/fablab-machine-logos.svg)

*Après s'est posée la question du rapport à la création ... !*

## Comment interpréter, faire évoluer ou réinventer un objet ? ##


C'est au travers de 5 mots *(référence, inspiration, hommage, infuence, extention)* et de leurs définitions dans des dictionnaires d'époques différentes *(Littré, Larousse, Robert, Wiktionnaire)* que les étudiants ont défini une attitude pour créer leur projet.

Prototypes, tests, essais de tout ordre ont balisé le chemin tumultueux de cette partie du travail. Nous leur avons demandé de tout documenter, même les choses qui leur paraissaient les plus futiles. Pour les aider à comprendre leur chemin, nous avons choisi au hasard des étudiants chaque semaine qui expliquaient au groupe la démarche qu'ils avaient suivie. Ces échanges ont permis à tous les étudiants de voir d'autres manières de penser et de faire. Cette manière de documenter a aussi permis à chacun de comprendre son propre fonctionnement et, par les petites victoires successives, de gagner en confiance pour arriver au projet final.

Grâce à ce mode de travail, l'énergie du groupe a augmenté de manière exponentielle. Les échanges étaient nombreux et des groupes d'entraide se sont formés spontanément tout au long du projet.

Habituellement, l'apprentissage commence par la théorie, ici c'est l’inverse : C'est en faisant, en testant, en se confrontant à la matière et à la fabrication d'un prototype fonctionnel que l'étudiant peut mieux appréhender la complexité et la richesse du travail d'autres designers.

*Equipe pour le cours de Design: Victor Lévy, Hélène Bardijn, Gwendoline Best*

*Equipe pour le module FabZero: Denis Terwagne, Nicolas Decoster, Thibaut Baes, Axel Cornu*


|   |   |
|---|---|
|  ![](./img/clementine.jpg) | [Clementine Benyakhou](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/clementine.benyakhou)   |
| ![](./img/alysee.jpg)  | [Alizee Cosyns](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/alizee.cosyns)   |
| ![](./img/margaux.jpg)  |  [Margaux Derclaye](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/margaux.derclaye)  |
|  ![](./img/dimitri.jpg) | [Dimitri Echallier](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/dimitri.echallier)  |
| ![](./img/lucas.jpg)  | [Lucas Etienne](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/lucas.etienne)   |
|  ![](./img/pierre.jpg) | [Pierre Langlois](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/pierre.langlois)   |
| ![](./img/noemie.jpg)  | [Noemie Laval](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/noemie.laval)  |
| ![](./img/maud.jpg)  | [Maud Maes](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maud.maes)   |
| ![](./img/lisa-marie.jpg)  | [Lisa-marie Merchat](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/lisa-marie.merchat)   |
|  ![](./img/jeanno.jpg) | [Jeano Mertens](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/jeano.mertens)   |
|  ![](./img/Nolwen.jpg) | [Nolwenn Meulders](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/nolwenn.meulders)  |
|  ![](./img/anthony.jpg) | [Anthony Millour](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/anthony.millour)   |
| ![](./img/elodie.jpg)  | [Elodie Nivet](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/elodie.nivet)   |
|  ![](./img/tejhay.jpg) | [Tejhay Pinheiroalbia](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/tejhay.pinheiroalbia)  |
|  ![](./img/flore.jpg) | [Flore Proye](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/flore.proye)   |
|  ![](./img/maxime.jpg) | [Maxime Wauthij](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij)   |
|  ![](./img/molly.jpg) | [Molly Scarfalotto](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/molly.scarfalotto)   |
|  ![](./img/manon.jpg) | [Manon Vanavermaete](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/manon.vanavermaete)   |
| ![](./img/theo.jpg)  | [Theo Dellicourt](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/theo.dellicourt)   |
|  ![](./img/nigel.jpg) | [Nigel Delhase](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/nigel.delhase)   |
|  ![](./img/laurens.jpg) | [Laurens Dellisse](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/laurens.dellisse)   |
|  ![](./img/abdelatif.jpg) | [Siari Abdellatif](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/siari.abdellatif)   |
|  ![](./img/fatima.jpg) | [Fatima Banga](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/fatima.banga)   |
|  ![](./img/cassandra.jpg) | [Cassandra Younes](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/cassandra.younes)  |
|  ![](./img/louise.jpg) | [Louise Vandeneynde](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/louise.vandeneynde)   |
|  ![](./img/simon.jpg) | [Simon Tirello](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/simon.tirello)   |
|  ![](./img/fabien.jpg) | [Fabien Pernot](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/fabien.pernot)  |
